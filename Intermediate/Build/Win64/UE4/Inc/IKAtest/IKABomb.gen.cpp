// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKABomb.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKABomb() {}
// Cross Module References
	IKATEST_API UFunction* Z_Construct_UFunction_AIKABomb_Explode();
	IKATEST_API UClass* Z_Construct_UClass_AIKABomb();
	IKATEST_API UClass* Z_Construct_UClass_AIKABomb_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
	IKATEST_API UClass* Z_Construct_UClass_AIKAtestCharacter_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AIKABomb_Explode = FName(TEXT("Explode"));
	void AIKABomb::Explode()
	{
		ProcessEvent(FindFunctionChecked(NAME_AIKABomb_Explode),NULL);
	}
	void AIKABomb::StaticRegisterNativesAIKABomb()
	{
	}
	UFunction* Z_Construct_UFunction_AIKABomb_Explode()
	{
		UObject* Outer = Z_Construct_UClass_AIKABomb();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Explode"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020800, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Actions"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AIKABomb_NoRegister()
	{
		return AIKABomb::StaticClass();
	}
	UClass* Z_Construct_UClass_AIKABomb()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = AIKABomb::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;

				OuterClass->LinkChild(Z_Construct_UFunction_AIKABomb_Explode());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(BombUnique, AIKABomb);
				UProperty* NewProp_BombUnique = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BombUnique"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(BombUnique, AIKABomb), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(BombUnique, AIKABomb), sizeof(bool), true);
				UProperty* NewProp_ExplosionRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionRange"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ExplosionRange, AIKABomb), 0x0010000000000005);
				UProperty* NewProp_OwnerPlayer = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OwnerPlayer"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OwnerPlayer, AIKABomb), 0x0010000000000005, Z_Construct_UClass_AIKAtestCharacter_NoRegister());
				UProperty* NewProp_BombMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BombMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(BombMesh, AIKABomb), 0x001200000008000d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKABomb_Explode(), "Explode"); // 2900344301
				static TCppClassTypeInfo<TCppClassTypeTraits<AIKABomb> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKABomb.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_BombUnique, TEXT("Category"), TEXT("Data"));
				MetaData->SetValue(NewProp_BombUnique, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
				MetaData->SetValue(NewProp_ExplosionRange, TEXT("Category"), TEXT("Data"));
				MetaData->SetValue(NewProp_ExplosionRange, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
				MetaData->SetValue(NewProp_OwnerPlayer, TEXT("Category"), TEXT("Data"));
				MetaData->SetValue(NewProp_OwnerPlayer, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
				MetaData->SetValue(NewProp_BombMesh, TEXT("Category"), TEXT("Visuals"));
				MetaData->SetValue(NewProp_BombMesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_BombMesh, TEXT("ModuleRelativePath"), TEXT("IKABomb.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIKABomb, 3760761237);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIKABomb(Z_Construct_UClass_AIKABomb, &AIKABomb::StaticClass, TEXT("/Script/IKAtest"), TEXT("AIKABomb"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIKABomb);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
