// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IKATEST_IKAController_generated_h
#error "IKAController.generated.h already included, missing '#pragma once' in IKAController.h"
#endif
#define IKATEST_IKAController_generated_h

#define IKAtest_Source_IKAtest_IKAController_h_15_RPC_WRAPPERS
#define IKAtest_Source_IKAtest_IKAController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define IKAtest_Source_IKAtest_IKAController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKAController(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAController(); \
public: \
	DECLARE_CLASS(AIKAController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAIKAController(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAController(); \
public: \
	DECLARE_CLASS(AIKAController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKAController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAController(AIKAController&&); \
	NO_API AIKAController(const AIKAController&); \
public:


#define IKAtest_Source_IKAtest_IKAController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKAController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAController(AIKAController&&); \
	NO_API AIKAController(const AIKAController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAController)


#define IKAtest_Source_IKAtest_IKAController_h_15_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKAController_h_12_PROLOG
#define IKAtest_Source_IKAtest_IKAController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAController_h_15_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAController_h_15_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKAController_h_15_INCLASS \
	IKAtest_Source_IKAtest_IKAController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKAController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAController_h_15_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAController_h_15_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKAController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
