// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKAGameViewportClient.h"
#include "Classes/Engine/Engine.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKAGameViewportClient() {}
// Cross Module References
	IKATEST_API UClass* Z_Construct_UClass_UIKAGameViewportClient_NoRegister();
	IKATEST_API UClass* Z_Construct_UClass_UIKAGameViewportClient();
	ENGINE_API UClass* Z_Construct_UClass_UGameViewportClient();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
// End Cross Module References
	void UIKAGameViewportClient::StaticRegisterNativesUIKAGameViewportClient()
	{
	}
	UClass* Z_Construct_UClass_UIKAGameViewportClient_NoRegister()
	{
		return UIKAGameViewportClient::StaticClass();
	}
	UClass* Z_Construct_UClass_UIKAGameViewportClient()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UGameViewportClient();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = UIKAGameViewportClient::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20100088u;


				static TCppClassTypeInfo<TCppClassTypeTraits<UIKAGameViewportClient> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKAGameViewportClient.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKAGameViewportClient.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIKAGameViewportClient, 655065792);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIKAGameViewportClient(Z_Construct_UClass_UIKAGameViewportClient, &UIKAGameViewportClient::StaticClass, TEXT("/Script/IKAtest"), TEXT("UIKAGameViewportClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIKAGameViewportClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
