// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKAMapBuilder.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKAMapBuilder() {}
// Cross Module References
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAMapBuilder_BuildMap();
	IKATEST_API UClass* Z_Construct_UClass_AIKAMapBuilder();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAMapBuilder_BuildMapProc();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAMapBuilder_ClearMap();
	IKATEST_API UClass* Z_Construct_UClass_AIKAMapBuilder_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void AIKAMapBuilder::StaticRegisterNativesAIKAMapBuilder()
	{
		UClass* Class = AIKAMapBuilder::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "BuildMap", (Native)&AIKAMapBuilder::execBuildMap },
			{ "BuildMapProc", (Native)&AIKAMapBuilder::execBuildMapProc },
			{ "ClearMap", (Native)&AIKAMapBuilder::execClearMap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_AIKAMapBuilder_BuildMap()
	{
		struct IKAMapBuilder_eventBuildMap_Parms
		{
			FString map;
		};
		UObject* Outer = Z_Construct_UClass_AIKAMapBuilder();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("BuildMap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(IKAMapBuilder_eventBuildMap_Parms));
			UProperty* NewProp_map = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("map"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(map, IKAMapBuilder_eventBuildMap_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Map"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAMapBuilder_BuildMapProc()
	{
		UObject* Outer = Z_Construct_UClass_AIKAMapBuilder();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("BuildMapProc"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Map"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAMapBuilder_ClearMap()
	{
		UObject* Outer = Z_Construct_UClass_AIKAMapBuilder();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ClearMap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Map"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AIKAMapBuilder_NoRegister()
	{
		return AIKAMapBuilder::StaticClass();
	}
	UClass* Z_Construct_UClass_AIKAMapBuilder()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = AIKAMapBuilder::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;

				OuterClass->LinkChild(Z_Construct_UFunction_AIKAMapBuilder_BuildMap());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAMapBuilder_BuildMapProc());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAMapBuilder_ClearMap());

				UProperty* NewProp_PlayerStartPos = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PlayerStartPos"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(PlayerStartPos, AIKAMapBuilder), 0x0010000000000014);
				UProperty* NewProp_PlayerStartPos_Inner = new(EC_InternalUseOnlyConstructor, NewProp_PlayerStartPos, TEXT("PlayerStartPos"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_DefaultMap = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DefaultMap"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(DefaultMap, AIKAMapBuilder), 0x0010000000000014);
				UProperty* NewProp_DefaultMap_Inner = new(EC_InternalUseOnlyConstructor, NewProp_DefaultMap, TEXT("DefaultMap"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				UProperty* NewProp_TyleTypes = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TyleTypes"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(TyleTypes, AIKAMapBuilder), 0x0010000000000005);
				UProperty* NewProp_TyleTypes_Inner = new(EC_InternalUseOnlyConstructor, NewProp_TyleTypes, TEXT("TyleTypes"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UClass_UObject_NoRegister(), Z_Construct_UClass_UClass());
				UProperty* NewProp_TileHeight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TileHeight"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(TileHeight, AIKAMapBuilder), 0x0010000000000005);
				UProperty* NewProp_TileWidth = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TileWidth"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(TileWidth, AIKAMapBuilder), 0x0010000000000005);
				UProperty* NewProp_MapHeight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MapHeight"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(MapHeight, AIKAMapBuilder), 0x0010000000000005);
				UProperty* NewProp_MapWidth = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MapWidth"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(MapWidth, AIKAMapBuilder), 0x0010000000000005);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAMapBuilder_BuildMap(), "BuildMap"); // 1666904035
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAMapBuilder_BuildMapProc(), "BuildMapProc"); // 2592060074
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAMapBuilder_ClearMap(), "ClearMap"); // 2721372415
				static TCppClassTypeInfo<TCppClassTypeTraits<AIKAMapBuilder> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_PlayerStartPos, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_PlayerStartPos, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_DefaultMap, TEXT("Category"), TEXT("Predefined Maps"));
				MetaData->SetValue(NewProp_DefaultMap, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_TyleTypes, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_TyleTypes, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_TileHeight, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_TileHeight, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_TileWidth, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_TileWidth, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_MapHeight, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_MapHeight, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
				MetaData->SetValue(NewProp_MapWidth, TEXT("Category"), TEXT("Map Properties"));
				MetaData->SetValue(NewProp_MapWidth, TEXT("ModuleRelativePath"), TEXT("IKAMapBuilder.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIKAMapBuilder, 1392845481);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIKAMapBuilder(Z_Construct_UClass_AIKAMapBuilder, &AIKAMapBuilder::StaticClass, TEXT("/Script/IKAtest"), TEXT("AIKAMapBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIKAMapBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
