// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IKATEST_IKAMapBuilder_generated_h
#error "IKAMapBuilder.generated.h already included, missing '#pragma once' in IKAMapBuilder.h"
#endif
#define IKATEST_IKAMapBuilder_generated_h

#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClearMap) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClearMap(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBuildMapProc) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BuildMapProc(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBuildMap) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_map); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BuildMap(Z_Param_map); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClearMap) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClearMap(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBuildMapProc) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BuildMapProc(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBuildMap) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_map); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BuildMap(Z_Param_map); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKAMapBuilder(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAMapBuilder(); \
public: \
	DECLARE_CLASS(AIKAMapBuilder, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAMapBuilder) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAIKAMapBuilder(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAMapBuilder(); \
public: \
	DECLARE_CLASS(AIKAMapBuilder, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAMapBuilder) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKAMapBuilder(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAMapBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAMapBuilder); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAMapBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAMapBuilder(AIKAMapBuilder&&); \
	NO_API AIKAMapBuilder(const AIKAMapBuilder&); \
public:


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAMapBuilder(AIKAMapBuilder&&); \
	NO_API AIKAMapBuilder(const AIKAMapBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAMapBuilder); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAMapBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAMapBuilder)


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKAMapBuilder_h_14_PROLOG
#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_INCLASS \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKAMapBuilder_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAMapBuilder_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKAMapBuilder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
