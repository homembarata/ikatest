// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
class AIKAtestCharacter;
#ifdef IKATEST_IKAPickUp_generated_h
#error "IKAPickUp.generated.h already included, missing '#pragma once' in IKAPickUp.h"
#endif
#define IKATEST_IKAPickUp_generated_h

#define IKAtest_Source_IKAtest_IKAPickUp_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_EVENT_PARMS \
	struct IKAPickUp_eventGivePowerUp_Parms \
	{ \
		AIKAtestCharacter* player; \
	};


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_CALLBACK_WRAPPERS
#define IKAtest_Source_IKAtest_IKAPickUp_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKAPickUp(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAPickUp(); \
public: \
	DECLARE_CLASS(AIKAPickUp, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAPickUp) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAIKAPickUp(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAPickUp(); \
public: \
	DECLARE_CLASS(AIKAPickUp, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAPickUp) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKAPickUp(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAPickUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAPickUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAPickUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAPickUp(AIKAPickUp&&); \
	NO_API AIKAPickUp(const AIKAPickUp&); \
public:


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAPickUp(AIKAPickUp&&); \
	NO_API AIKAPickUp(const AIKAPickUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAPickUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAPickUp); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAPickUp)


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKAPickUp_h_11_PROLOG \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_EVENT_PARMS


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_CALLBACK_WRAPPERS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_INCLASS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKAPickUp_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_CALLBACK_WRAPPERS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAPickUp_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKAPickUp_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
