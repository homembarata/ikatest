// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKATile.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKATile() {}
// Cross Module References
	IKATEST_API UClass* Z_Construct_UClass_AIKATile_NoRegister();
	IKATEST_API UClass* Z_Construct_UClass_AIKATile();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AIKATile::StaticRegisterNativesAIKATile()
	{
	}
	UClass* Z_Construct_UClass_AIKATile_NoRegister()
	{
		return AIKATile::StaticClass();
	}
	UClass* Z_Construct_UClass_AIKATile()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = AIKATile::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;


				UProperty* NewProp_TileMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TileMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TileMesh, AIKATile), 0x001200000008000d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				static TCppClassTypeInfo<TCppClassTypeTraits<AIKATile> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKATile.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKATile.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_TileMesh, TEXT("Category"), TEXT("Visuals"));
				MetaData->SetValue(NewProp_TileMesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_TileMesh, TEXT("ModuleRelativePath"), TEXT("IKATile.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIKATile, 3348066578);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIKATile(Z_Construct_UClass_AIKATile, &AIKATile::StaticClass, TEXT("/Script/IKAtest"), TEXT("AIKATile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIKATile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
