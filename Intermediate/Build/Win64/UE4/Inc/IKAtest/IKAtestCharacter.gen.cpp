// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKAtestCharacter.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKAtestCharacter() {}
// Cross Module References
	IKATEST_API UFunction* Z_Construct_UDelegateFunction_IKAtest_CharacterDamageSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_MoveRight();
	IKATEST_API UClass* Z_Construct_UClass_AIKAtestCharacter();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_MoveUp();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_OnBomb();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_RangeUp();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_RecoverBomb();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAtestCharacter_SpeedUp();
	IKATEST_API UClass* Z_Construct_UClass_AIKAtestCharacter_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	UFunction* Z_Construct_UDelegateFunction_IKAtest_CharacterDamageSignature__DelegateSignature()
	{
		UObject* Outer = Z_Construct_UPackage__Script_IKAtest();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CharacterDamageSignature__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00130000, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	void AIKAtestCharacter::StaticRegisterNativesAIKAtestCharacter()
	{
		UClass* Class = AIKAtestCharacter::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "MoveRight", (Native)&AIKAtestCharacter::execMoveRight },
			{ "MoveUp", (Native)&AIKAtestCharacter::execMoveUp },
			{ "OnBomb", (Native)&AIKAtestCharacter::execOnBomb },
			{ "RangeUp", (Native)&AIKAtestCharacter::execRangeUp },
			{ "RecoverBomb", (Native)&AIKAtestCharacter::execRecoverBomb },
			{ "SpeedUp", (Native)&AIKAtestCharacter::execSpeedUp },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_MoveRight()
	{
		struct IKAtestCharacter_eventMoveRight_Parms
		{
			float Val;
		};
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MoveRight"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00020401, 65535, sizeof(IKAtestCharacter_eventMoveRight_Parms));
			UProperty* NewProp_Val = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Val"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Val, IKAtestCharacter_eventMoveRight_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_MoveUp()
	{
		struct IKAtestCharacter_eventMoveUp_Parms
		{
			float Val;
		};
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MoveUp"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00020401, 65535, sizeof(IKAtestCharacter_eventMoveUp_Parms));
			UProperty* NewProp_Val = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Val"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Val, IKAtestCharacter_eventMoveUp_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_OnBomb()
	{
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnBomb"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_RangeUp()
	{
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RangeUp"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Gameplay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_RecoverBomb()
	{
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RecoverBomb"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Gameplay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAtestCharacter_SpeedUp()
	{
		UObject* Outer = Z_Construct_UClass_AIKAtestCharacter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SpeedUp"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Gameplay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AIKAtestCharacter_NoRegister()
	{
		return AIKAtestCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_AIKAtestCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = AIKAtestCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20800080u;

				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_MoveRight());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_MoveUp());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_OnBomb());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_RangeUp());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_RecoverBomb());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAtestCharacter_SpeedUp());

				UProperty* NewProp_OnReceiveDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnReceiveDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnReceiveDamage, AIKAtestCharacter), 0x0010000010080000, Z_Construct_UDelegateFunction_IKAtest_CharacterDamageSignature__DelegateSignature());
				UProperty* NewProp_BombRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BombRange"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(BombRange, AIKAtestCharacter), 0x0010000000020005);
				UProperty* NewProp_BombsDropped = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BombsDropped"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(BombsDropped, AIKAtestCharacter), 0x0010000000020005);
				UProperty* NewProp_MaxBombs = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxBombs"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(MaxBombs, AIKAtestCharacter), 0x0010000000020005);
				UProperty* NewProp_MoveSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MoveSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MoveSpeed, AIKAtestCharacter), 0x0010000000020005);
				UProperty* NewProp_BombType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BombType"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(BombType, AIKAtestCharacter), 0x0010000000020005, Z_Construct_UClass_UObject_NoRegister(), Z_Construct_UClass_UClass());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_MoveRight(), "MoveRight"); // 1696770452
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_MoveUp(), "MoveUp"); // 3623758102
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_OnBomb(), "OnBomb"); // 4021657844
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_RangeUp(), "RangeUp"); // 2212125808
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_RecoverBomb(), "RecoverBomb"); // 2200600255
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAtestCharacter_SpeedUp(), "SpeedUp"); // 3668723378
				static TCppClassTypeInfo<TCppClassTypeTraits<AIKAtestCharacter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_OnReceiveDamage, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_OnReceiveDamage, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_BombRange, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_BombRange, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_BombsDropped, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_BombsDropped, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_MaxBombs, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_MaxBombs, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_MoveSpeed, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_MoveSpeed, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
				MetaData->SetValue(NewProp_BombType, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_BombType, TEXT("ModuleRelativePath"), TEXT("IKAtestCharacter.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIKAtestCharacter, 798089995);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIKAtestCharacter(Z_Construct_UClass_AIKAtestCharacter, &AIKAtestCharacter::StaticClass, TEXT("/Script/IKAtest"), TEXT("AIKAtestCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIKAtestCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
