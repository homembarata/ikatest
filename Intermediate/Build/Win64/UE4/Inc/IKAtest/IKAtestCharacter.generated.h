// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IKATEST_IKAtestCharacter_generated_h
#error "IKAtestCharacter.generated.h already included, missing '#pragma once' in IKAtestCharacter.h"
#endif
#define IKATEST_IKAtestCharacter_generated_h

#define IKAtest_Source_IKAtest_IKAtestCharacter_h_9_DELEGATE \
static inline void FCharacterDamageSignature_DelegateWrapper(const FMulticastScriptDelegate& CharacterDamageSignature) \
{ \
	CharacterDamageSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRangeUp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RangeUp(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpeedUp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpeedUp(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRecoverBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RecoverBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Val); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MoveRight(Z_Param_Val); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveUp) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Val); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MoveUp(Z_Param_Val); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRangeUp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RangeUp(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpeedUp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpeedUp(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRecoverBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RecoverBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Val); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MoveRight(Z_Param_Val); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveUp) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Val); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MoveUp(Z_Param_Val); \
		P_NATIVE_END; \
	}


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKAtestCharacter(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAtestCharacter(); \
public: \
	DECLARE_CLASS(AIKAtestCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAtestCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAIKAtestCharacter(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAtestCharacter(); \
public: \
	DECLARE_CLASS(AIKAtestCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKAtestCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKAtestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAtestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAtestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAtestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAtestCharacter(AIKAtestCharacter&&); \
	NO_API AIKAtestCharacter(const AIKAtestCharacter&); \
public:


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKAtestCharacter(AIKAtestCharacter&&); \
	NO_API AIKAtestCharacter(const AIKAtestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKAtestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAtestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AIKAtestCharacter)


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKAtestCharacter_h_11_PROLOG
#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_INCLASS \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKAtestCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAtestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKAtestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
