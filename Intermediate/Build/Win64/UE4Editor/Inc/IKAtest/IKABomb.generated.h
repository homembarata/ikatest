// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IKATEST_IKABomb_generated_h
#error "IKABomb.generated.h already included, missing '#pragma once' in IKABomb.h"
#endif
#define IKATEST_IKABomb_generated_h

#define IKAtest_Source_IKAtest_IKABomb_h_13_RPC_WRAPPERS
#define IKAtest_Source_IKAtest_IKABomb_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define IKAtest_Source_IKAtest_IKABomb_h_13_EVENT_PARMS
#define IKAtest_Source_IKAtest_IKABomb_h_13_CALLBACK_WRAPPERS
#define IKAtest_Source_IKAtest_IKABomb_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKABomb(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKABomb(); \
public: \
	DECLARE_CLASS(AIKABomb, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKABomb) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKABomb_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAIKABomb(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKABomb(); \
public: \
	DECLARE_CLASS(AIKABomb, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/IKAtest"), NO_API) \
	DECLARE_SERIALIZER(AIKABomb) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKABomb_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIKABomb(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKABomb) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKABomb); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKABomb(AIKABomb&&); \
	NO_API AIKABomb(const AIKABomb&); \
public:


#define IKAtest_Source_IKAtest_IKABomb_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIKABomb(AIKABomb&&); \
	NO_API AIKABomb(const AIKABomb&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIKABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKABomb); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKABomb)


#define IKAtest_Source_IKAtest_IKABomb_h_13_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKABomb_h_10_PROLOG \
	IKAtest_Source_IKAtest_IKABomb_h_13_EVENT_PARMS


#define IKAtest_Source_IKAtest_IKABomb_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKABomb_h_13_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKABomb_h_13_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKABomb_h_13_CALLBACK_WRAPPERS \
	IKAtest_Source_IKAtest_IKABomb_h_13_INCLASS \
	IKAtest_Source_IKAtest_IKABomb_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKABomb_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKABomb_h_13_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKABomb_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKABomb_h_13_CALLBACK_WRAPPERS \
	IKAtest_Source_IKAtest_IKABomb_h_13_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKABomb_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKABomb_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
