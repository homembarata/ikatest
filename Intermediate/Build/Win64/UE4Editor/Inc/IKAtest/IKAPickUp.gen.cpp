// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "IKAPickUp.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIKAPickUp() {}
// Cross Module References
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAPickUp_GivePowerUp();
	IKATEST_API UClass* Z_Construct_UClass_AIKAPickUp();
	IKATEST_API UClass* Z_Construct_UClass_AIKAtestCharacter_NoRegister();
	IKATEST_API UFunction* Z_Construct_UFunction_AIKAPickUp_OnPlayerOverlap();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	IKATEST_API UClass* Z_Construct_UClass_AIKAPickUp_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_IKAtest();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperSpriteComponent_NoRegister();
// End Cross Module References
	static FName NAME_AIKAPickUp_GivePowerUp = FName(TEXT("GivePowerUp"));
	void AIKAPickUp::GivePowerUp(AIKAtestCharacter* player)
	{
		IKAPickUp_eventGivePowerUp_Parms Parms;
		Parms.player=player;
		ProcessEvent(FindFunctionChecked(NAME_AIKAPickUp_GivePowerUp),&Parms);
	}
	void AIKAPickUp::StaticRegisterNativesAIKAPickUp()
	{
		UClass* Class = AIKAPickUp::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "OnPlayerOverlap", (Native)&AIKAPickUp::execOnPlayerOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_AIKAPickUp_GivePowerUp()
	{
		UObject* Outer = Z_Construct_UClass_AIKAPickUp();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GivePowerUp"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020800, 65535, sizeof(IKAPickUp_eventGivePowerUp_Parms));
			UProperty* NewProp_player = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("player"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(player, IKAPickUp_eventGivePowerUp_Parms), 0x0010000000000080, Z_Construct_UClass_AIKAtestCharacter_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Actions"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAPickUp.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AIKAPickUp_OnPlayerOverlap()
	{
		struct IKAPickUp_eventOnPlayerOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
		UObject* Outer = Z_Construct_UClass_AIKAPickUp();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnPlayerOverlap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00420401, 65535, sizeof(IKAPickUp_eventOnPlayerOverlap_Parms));
			UProperty* NewProp_SweepResult = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SweepResult"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SweepResult, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010008008000182, Z_Construct_UScriptStruct_FHitResult());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFromSweep, IKAPickUp_eventOnPlayerOverlap_Parms);
			UProperty* NewProp_bFromSweep = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bFromSweep"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFromSweep, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(bFromSweep, IKAPickUp_eventOnPlayerOverlap_Parms), sizeof(bool), true);
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_OverlappedComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverlappedComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OverlappedComp, IKAPickUp_eventOnPlayerOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("IKAPickUp.h"));
			MetaData->SetValue(NewProp_SweepResult, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_OverlappedComp, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AIKAPickUp_NoRegister()
	{
		return AIKAPickUp::StaticClass();
	}
	UClass* Z_Construct_UClass_AIKAPickUp()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_IKAtest();
			OuterClass = AIKAPickUp::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;

				OuterClass->LinkChild(Z_Construct_UFunction_AIKAPickUp_GivePowerUp());
				OuterClass->LinkChild(Z_Construct_UFunction_AIKAPickUp_OnPlayerOverlap());

				UProperty* NewProp_CollisionBox = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CollisionBox"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CollisionBox, AIKAPickUp), 0x001200000008000d, Z_Construct_UClass_UBoxComponent_NoRegister());
				UProperty* NewProp_Sprite = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Sprite"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Sprite, AIKAPickUp), 0x001200000008000d, Z_Construct_UClass_UPaperSpriteComponent_NoRegister());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAPickUp_GivePowerUp(), "GivePowerUp"); // 4052444507
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AIKAPickUp_OnPlayerOverlap(), "OnPlayerOverlap"); // 3688147487
				static TCppClassTypeInfo<TCppClassTypeTraits<AIKAPickUp> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("IKAPickUp.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("IKAPickUp.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_CollisionBox, TEXT("Category"), TEXT("Collision"));
				MetaData->SetValue(NewProp_CollisionBox, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CollisionBox, TEXT("ModuleRelativePath"), TEXT("IKAPickUp.h"));
				MetaData->SetValue(NewProp_Sprite, TEXT("Category"), TEXT("Visuals"));
				MetaData->SetValue(NewProp_Sprite, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Sprite, TEXT("ModuleRelativePath"), TEXT("IKAPickUp.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIKAPickUp, 257573519);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIKAPickUp(Z_Construct_UClass_AIKAPickUp, &AIKAPickUp::StaticClass, TEXT("/Script/IKAtest"), TEXT("AIKAPickUp"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIKAPickUp);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
