// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IKATEST_IKAtestGameMode_generated_h
#error "IKAtestGameMode.generated.h already included, missing '#pragma once' in IKAtestGameMode.h"
#endif
#define IKATEST_IKAtestGameMode_generated_h

#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_RPC_WRAPPERS
#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIKAtestGameMode(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAtestGameMode(); \
public: \
	DECLARE_CLASS(AIKAtestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/IKAtest"), IKATEST_API) \
	DECLARE_SERIALIZER(AIKAtestGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAIKAtestGameMode(); \
	friend IKATEST_API class UClass* Z_Construct_UClass_AIKAtestGameMode(); \
public: \
	DECLARE_CLASS(AIKAtestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/IKAtest"), IKATEST_API) \
	DECLARE_SERIALIZER(AIKAtestGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IKATEST_API AIKAtestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAtestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IKATEST_API, AIKAtestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAtestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IKATEST_API AIKAtestGameMode(AIKAtestGameMode&&); \
	IKATEST_API AIKAtestGameMode(const AIKAtestGameMode&); \
public:


#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IKATEST_API AIKAtestGameMode(AIKAtestGameMode&&); \
	IKATEST_API AIKAtestGameMode(const AIKAtestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IKATEST_API, AIKAtestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIKAtestGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIKAtestGameMode)


#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_PRIVATE_PROPERTY_OFFSET
#define IKAtest_Source_IKAtest_IKAtestGameMode_h_10_PROLOG
#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_RPC_WRAPPERS \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_INCLASS \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IKAtest_Source_IKAtest_IKAtestGameMode_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_INCLASS_NO_PURE_DECLS \
	IKAtest_Source_IKAtest_IKAtestGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IKAtest_Source_IKAtest_IKAtestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
