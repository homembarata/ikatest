// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IKAtestCharacter.h"
#include "GameFramework/Actor.h"
#include "IKABomb.generated.h"

UCLASS()
class IKATEST_API AIKABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIKABomb();
	AIKABomb(const FObjectInitializer &ObjectInitializer);

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Visuals")
	UStaticMeshComponent *BombMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	AIKAtestCharacter *OwnerPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	float ExplosionRange = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	bool BombUnique = false;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Actions")
	void Explode();
};
