// Fill out your copyright notice in the Description page of Project Settings.

#include "IKAController.h"

#include "IKAtestCharacter.h"



void AIKAController::SetupInputComponent()
{
	Super::SetupInputComponent();

	int32 controllerId = GetLocalPlayer()->GetControllerId();

	if (controllerId == 0)
	{
		InputComponent->BindAxis("MoveUp_P1", this, &AIKAController::MoveUp);
		InputComponent->BindAxis("MoveRight_P1", this, &AIKAController::MoveRight);
	}
	else if (controllerId == 1)
	{
		InputComponent->BindAxis("MoveUp_P2", this, &AIKAController::MoveUp);
		InputComponent->BindAxis("MoveRight_P2", this, &AIKAController::MoveRight);


	}
}

void AIKAController::MoveUp(float Val)
{
	//	if (!IsDead && (Controller != NULL) && (Val != 0.f))
	{
		if (AIKAtestCharacter* MyPawn = Cast<AIKAtestCharacter>(GetPawn()))
		{
			const FVector Direction = FVector(1, 0, 0);

			MyPawn->AddMovementInput(Direction, Val*MyPawn->MoveSpeed);
		}
	}
}

void AIKAController::MoveRight(float Val)
{
	//	if (!IsDead && (Controller != NULL) && (Val != 0.f))
	{
		if (AIKAtestCharacter* MyPawn = Cast<AIKAtestCharacter>(GetPawn()))
		{
			const FVector Direction = FVector(0, 1, 0);

			MyPawn->AddMovementInput(Direction, Val*MyPawn->MoveSpeed);
		}
	}
}