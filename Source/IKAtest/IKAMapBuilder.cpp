// Fill out your copyright notice in the Description page of Project Settings.

#include "IKAMapBuilder.h"

AIKAMapBuilder::AIKAMapBuilder()
{

}

AIKAMapBuilder::AIKAMapBuilder(const FObjectInitializer &ObjectInitializer)
	: Super(ObjectInitializer)
{
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("RootComponent"));
}

void AIKAMapBuilder::BuildMap(FString map)
{
	TArray<FString> TilesArray;

	map.Replace(TEXT("\n"), TEXT("")).ParseIntoArray(TilesArray, TEXT(","));

	for (int32 index = 0; index < TilesArray.Num(); index++)
	{
		int32 CharIndex;
		if (TilesArray[index].FindChar('X', CharIndex))
		{
			FVector location = this->GetActorLocation();

			location.Y += ((index%MapWidth)*TileWidth);
			location.X -= ((index / MapWidth)*TileHeight);

			PlayerStartPos.Add(location);
		}
		else
		{
			int32 typeIndex = FCString::Atoi(*TilesArray[index]);

			AIKATile *tile = Cast<AIKATile>(GetWorld()->SpawnActor(TyleTypes[typeIndex]));

			if (tile)
			{
				FVector location = this->GetActorLocation();

				location.Y += ((index%MapWidth)*TileWidth);
				location.X -= ((index / MapWidth)*TileHeight);

				tile->SetActorLocation(location);
			}
		}
		
	}
}

void AIKAMapBuilder::BuildMapProc()
{
	for (int32 i = 0; i < MapHeight; i++)
	{
		for (int32 j = 0; j < MapWidth; j++)
		{
			int32 index = FMath::RandRange(0, TyleTypes.Num() - 1);
			if (TyleTypes[index])
			{
				AIKATile *tile = Cast<AIKATile>(GetWorld()->SpawnActor(TyleTypes[index]));

				if (tile)
				{
					FVector location = this->GetActorLocation();
					location.X += (j*TileWidth);
					location.Y += (i*TileHeight);

					tile->SetActorLocation(location);
				}
			}
		}
	}
}

void AIKAMapBuilder::ClearMap()
{

}