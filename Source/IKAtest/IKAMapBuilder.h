// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IKATile.h"
#include "UObject/NoExportTypes.h"
#include "IKAMapBuilder.generated.h"

/**
 * 
 */

UCLASS()
class IKATEST_API AIKAMapBuilder : public AActor
{
	GENERATED_BODY()

public:
	AIKAMapBuilder();
	AIKAMapBuilder(const FObjectInitializer &ObjectInitializer);
	
	UFUNCTION(BlueprintCallable, Category = "Map")
	void BuildMap(FString map);

	UFUNCTION(BlueprintCallable, Category = "Map")
	void BuildMapProc();

	UFUNCTION(BlueprintCallable, Category = "Map")
	void ClearMap();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Properties")
	int32 MapWidth = 13;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Properties")
	int32 MapHeight = 11;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Properties")
	int32 TileWidth = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Properties")
	int32 TileHeight = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Properties")
	TArray<UClass *> TyleTypes;

	UPROPERTY(BlueprintReadOnly, Category = "Predefined Maps")
	TArray<int32> DefaultMap;

	UPROPERTY(BlueprintReadOnly, Category = "Map Properties")
	TArray<FVector> PlayerStartPos;

private:
	TArray<AIKATile *> TilesReference;
};
