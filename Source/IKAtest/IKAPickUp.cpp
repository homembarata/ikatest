// Fill out your copyright notice in the Description page of Project Settings.

#include "IKAPickUp.h"
#include "IKAtestCharacter.h"



// Sets default values
AIKAPickUp::AIKAPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

AIKAPickUp::AIKAPickUp(const FObjectInitializer &ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	CollisionBox = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxComp"));

	FVector boxSize;
	boxSize.X = 100.f;
	boxSize.Y = 100.f;
	boxSize.Z = 100.f;

	CollisionBox->InitBoxExtent(boxSize);

	Sprite = ObjectInitializer.CreateDefaultSubobject<UPaperSpriteComponent>(this, TEXT("Sprite"));

	this->RootComponent = CollisionBox;
	Sprite->SetupAttachment(this->RootComponent);

	Sprite->SetWorldRotation(FRotator(0, 0, 90.f));
}

// Called when the game starts or when spawned
void AIKAPickUp::BeginPlay()
{
	Super::BeginPlay();

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AIKAPickUp::OnPlayerOverlap);
}

// Called every frame
void AIKAPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AIKAPickUp::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	// Call the base class - this will tell us how much damage to apply  
	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		this->Destroy();
	}

	return ActualDamage;
}


void AIKAPickUp::OnPlayerOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr)
	{
		if (AIKAtestCharacter *player = Cast<AIKAtestCharacter>(OtherActor))
		{
			GivePowerUp(player);
		}
	}
}
