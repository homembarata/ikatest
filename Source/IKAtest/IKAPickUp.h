// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "PaperSpriteComponent.h"
#include "GameFramework/Actor.h"
#include "IKAPickUp.generated.h"

UCLASS()
class IKATEST_API AIKAPickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIKAPickUp();
	AIKAPickUp(const FObjectInitializer &ObjectInitializer);

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Visuals")
	UPaperSpriteComponent *Sprite;

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Collision")
	UBoxComponent *CollisionBox;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UFUNCTION()
	void OnPlayerOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Actions")
	void GivePowerUp(AIKAtestCharacter *player);
};
