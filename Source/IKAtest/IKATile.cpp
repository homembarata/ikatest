// Fill out your copyright notice in the Description page of Project Settings.

#include "IKATile.h"


// Sets default values
AIKATile::AIKATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

AIKATile::AIKATile(const FObjectInitializer &ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	TileMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("TileMesh"));

	this->RootComponent = TileMesh;
}

// Called when the game starts or when spawned
void AIKATile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIKATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

