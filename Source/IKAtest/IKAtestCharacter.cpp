// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "IKAtestCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "IKABomb.h"
#include "Materials/Material.h"

AIKAtestCharacter::AIKAtestCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AIKAtestCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void AIKAtestCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponentRef)
{
	Super::SetupPlayerInputComponent(InputComponent);
	
	int32 controllerId = GetController()->CastToPlayerController()->GetLocalPlayer()->GetControllerId();

	if (controllerId == 0)
	{
		InputComponentRef->BindAction("Bomb_P1", IE_Pressed, this, &AIKAtestCharacter::OnBomb);
	}
	else if (controllerId == 1)
	{
		InputComponentRef->BindAction("Bomb_P2", IE_Pressed, this, &AIKAtestCharacter::OnBomb);
	}
}


float AIKAtestCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		this->RootComponent->SetVisibility(false, true);
		this->Dead = true;

		if (OnReceiveDamage.IsBound())
		{
			OnReceiveDamage.Broadcast();
		}
	}

	return ActualDamage;
}

void AIKAtestCharacter::MoveUp(float Val)
{
//	if (!IsDead && (Controller != NULL) && (Val != 0.f))
	{
		const FVector Direction = FVector(1, 0, 0);

		AddMovementInput(Direction, Val*MoveSpeed);
	}
}

void AIKAtestCharacter::MoveRight(float Val)
{
	//if (!IsDead && (Controller != NULL) && (Val != 0.f))
	{
		const FVector Direction = FVector(0, 1, 0);

		AddMovementInput(Direction, Val*MoveSpeed);
	}
}

void AIKAtestCharacter::OnBomb()
{
	UE_LOG(LogTemp, Warning, TEXT("BombsDropped %d MaxBombs %d"), BombsDropped, MaxBombs);
	if (BombsDropped < MaxBombs)
	{
		FActorSpawnParameters params;
		params.Owner = this;

		const FVector location = this->GetActorLocation();
		AIKABomb *bomb = Cast<AIKABomb>(GetWorld()->SpawnActor(BombType, &location, &FRotator::ZeroRotator, params));

		if (bomb)
		{
			bomb->OwnerPlayer = this;
			bomb->ExplosionRange = this->BombRange;
			//bomb->SetActorLocation(this->GetActorLocation());
			BombsDropped++;
		}
	}

	if (OnActionButton.IsBound())
	{
		OnActionButton.Broadcast();
	}

}

void AIKAtestCharacter::RecoverBomb()
{
	UE_LOG(LogTemp, Warning, TEXT("Recover %d"), BombsDropped);
	if (BombsDropped > 0)
	{
		BombsDropped--;
	}
}

void AIKAtestCharacter::SpeedUp()
{
	MoveSpeed += 2.f;
}

void AIKAtestCharacter::RangeUp()
{
	BombRange += 200.f;
}

void AIKAtestCharacter::MaxBombsUp()
{
	if (MaxBombs < BombLimit)
	{
		MaxBombs++;
	}
}

void AIKAtestCharacter::SetBombLimit(int32 newLimit)
{
	BombLimit = newLimit;
	MaxBombs = BombLimit;
}