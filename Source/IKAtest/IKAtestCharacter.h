// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IKAtestCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterDamageSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterActionButtonSignature);

UCLASS(Blueprintable)
class AIKAtestCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AIKAtestCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponentRef) override;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UFUNCTION()
	void MoveUp(float Val);

	UFUNCTION()
	void MoveRight(float Val);

	UFUNCTION()
	void OnBomb();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	UClass *BombType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	float MoveSpeed = 1.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 MaxBombs = 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 BombLimit = 8;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 BombsDropped = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	float BombRange = 200.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool Dead = false;

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void RecoverBomb();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void SpeedUp();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void RangeUp();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void MaxBombsUp();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void SetBombLimit(int32 newLimit);

	UPROPERTY(BlueprintAssignable, Category = Gameplay)
	FCharacterDamageSignature OnReceiveDamage;

	UPROPERTY(BlueprintAssignable, Category = Gameplay)
	FCharacterActionButtonSignature OnActionButton;
};

