// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "IKAtestGameMode.h"
#include "IKAtestCharacter.h"
#include "IKAController.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

AIKAtestGameMode::AIKAtestGameMode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	// use our custom PlayerController class
	PlayerControllerClass = AIKAController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

}

AIKAtestGameMode::AIKAtestGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AIKAController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

}

void AIKAtestGameMode::StartPlay()
{
	UWorld *world = this->GetWorld();
	UGameplayStatics::CreatePlayer(this->GetWorld(), -1, true);
	
	Super::StartPlay();
}

APawn *AIKAtestGameMode::SpawnDefaultPawnFor_Implementation(AController *NewPlayer, AActor *StartSpot)
{
	return nullptr;
}