// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IKAMapBuilder.h"
#include "IKAtestGameMode.generated.h"

UCLASS(minimalapi)
class AIKAtestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AIKAtestGameMode();
	AIKAtestGameMode(const FObjectInitializer& ObjectInitializer);

	void StartPlay() override;

	APawn *SpawnDefaultPawnFor_Implementation(AController *NewPlayer, AActor *StartSpot) override;
};



